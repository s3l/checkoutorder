CheckoutOrder by Stewart Levine - https://gitlab.com/s3l/checkoutorder

Written in Java 8 using Gradle 5.4 to manage dependencies.

Clone the project locally and switch to the project root directory to issue any of the following the commands
 gradle build --refresh-dependencies
 gradle test
 java -jar build/libs/CheckoutOrder.jar

A command-line menu is provided for user input. When entering an item, markdown or special, it is designed to take one input after another separated by 'Enter'. Hitting 'Enter' a second time backs it out to the previous menu.