package app;

import java.util.Scanner;

import store.ProductConfigurator;
import store.Register;
import ui.MainMenu;

public class CheckoutOrderTotalApp {
	

	public static void main(String[] args) {
		
		ProductConfigurator configurator = new ProductConfigurator();
		Register register = new Register(configurator);
		Scanner scanner = new Scanner(System.in);
		
		try { 
			new MainMenu().loadMenu(register, configurator, scanner);
			
		} finally {
			scanner.close();
		}

	}

}
