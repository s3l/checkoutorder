package checkout;

public class Buy_N_For_X_Special extends Special {
	
	private Double price;

	public Buy_N_For_X_Special(String productName) {
		super(productName);
		setLimit(0.0);
	}
	
	public double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return getProductName() + 
				": Buy " + getRequiredQuantity() + 
				" for $" + price +
				displayLimit();
	}
	
	
}
