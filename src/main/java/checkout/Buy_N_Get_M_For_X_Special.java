package checkout;

public class Buy_N_Get_M_For_X_Special extends Special {
	
	private Double eligibleQuantity;
	private Double percentageDiscount;

	public Buy_N_Get_M_For_X_Special(String productName) {
		super(productName);
		setLimit(0.0);
	}

	public Double getPercentageDiscount() {
		return percentageDiscount;
	}
	public void setPercentageDiscount(Double percentageDiscount) {
		this.percentageDiscount = percentageDiscount;
	}
	public Double getEligibleQuantity() {
		return eligibleQuantity;
	}
	public void setEligibleQuantity(Double eligibleQuantity) {
		this.eligibleQuantity = eligibleQuantity;
	}
	
	@Override
	public String toString() {
		return getProductName() + 
				": Buy " + getRequiredQuantity() + 
				", Get " + eligibleQuantity + 
				" for %" + percentageDiscount * 100 + " off" + 
				displayLimit();
	}

}
