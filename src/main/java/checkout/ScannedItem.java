package checkout;

import product.Item;
import product.Measure;

public class ScannedItem {

	private Item item;
	private Double quantity;
	private Double extendedQuantity;
	private Double extendedPrice;
	
	public ScannedItem(Item item) {
		this.item = item;
		quantity = new Double(0);
		extendedPrice = new Double(0);
		extendedQuantity = new Double(0);
	}
	
	public Item getItem() {
		return item;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void increaseQuantity(Double quantity) {
		this.quantity += quantity;
	}
	public void setQuantity(Double quantity) {
		if(Measure.EACH == item.getMeasure()) {
			String[] tokens = String.valueOf(quantity).split("\\.");
			double wholeNumber = Double.valueOf(tokens[0]);
			this.quantity = wholeNumber;
		} else {
			this.quantity = quantity;
		}
	}
	
	public Double getExtendedQuantity() {
		return extendedQuantity;
	}
	public void setExtendedQuantity(Double extendedQuantity) {
		this.extendedQuantity = extendedQuantity;
	}
	public Double getExtendedPrice() {
		return extendedPrice;
	}
	public void setExtendendPrice(Double extendedPrice) {
		this.extendedPrice = extendedPrice;
	}

}
