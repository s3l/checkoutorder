package checkout;

public abstract class Special {
	
	private String productName;
	private Double requiredQuantity;
	private Double limit;

	public Special(String productName) {
		this.productName = productName;
	}

	public String getProductName() {
		return productName;
	}
	public Double getRequiredQuantity() {
		return requiredQuantity;
	}
	public void setRequiredQuantity(Double requiredQuantity) {
		this.requiredQuantity = requiredQuantity;
	}
	public Double getLimit() {
		return limit;
	}
	public void setLimit(Double limit) {
		this.limit = limit;
	}
	public String displayLimit() {
		if(limit==0) 
			return " no limit";
		
		return " limit of " + limit;
	}
	
}
