package product;

public class Item {
	
	private String name;
	private Measure measure;
	private Double price;

	public Item(String name, Measure measure) {
		if(name==null || name.trim().length()==0)
			throw new IllegalArgumentException("Item name cannot be null or blank");
		if(measure==null) {
			throw new IllegalArgumentException("Item measure cannot be null");
		}
		this.name = name;
		this.measure = measure;
	}

	public String getName() {
		return name;
	}
	public Measure getMeasure() {
		return measure;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return name + " at $" + price + " " + measure;
	}

}
