package store;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import checkout.Buy_N_For_X_Special;
import checkout.Buy_N_Get_M_For_X_Special;
import checkout.Special;
import product.Item;
import product.Measure;

public class ProductConfigurator {
	
	private Map<String,Item> storeItems;
	private Map<String, Double> markdowns;
	private Map<String, Special> specials;
	
	public ProductConfigurator() {
		storeItems = new HashMap<>();
		markdowns = new HashMap<>();
		specials = new HashMap<>();
	}
	
	public Map<String,Item> getItems() {
		return storeItems;
	}

	public void addItemToStore(Item item) {
		if (item.getPrice()==null || item.getPrice() <= 0)
			throw new IllegalStateException("Price must be greater than zero");
		if (storeItems.containsKey(item.getName()))
			throw new IllegalArgumentException("Item already exists with that name");
		
		storeItems.put(item.getName(),item);
	}

	public Item enterNewItem(String input) {
		Item item = null;
		String[] tokens = tokenizeNewItemEntryInput(input);
		
		String name = tokens[0].trim();
		Measure measure = Measure.valueOf(tokens[1].trim().toUpperCase());
		Double price = Double.valueOf(tokens[2]);
		
		item = new Item(name, measure);
		item.setPrice(price);
		
		return item;
	}
	
	
	private static String[] tokenizeNewItemEntryInput(String input) {
		String[] tokens = input.split(",");
		if(input.trim().length()==0 || tokens.length!=3) {
			throw new IllegalArgumentException("Use input format: 'name,measure,price'");
		}
		return tokens;
	}

	public Item getItemByName(String itemName) {
		Item item = storeItems.get(itemName);
		if(item==null)
			throw new NoSuchElementException("No item found with name: " + itemName);
		return item;
	}
	
	public void removeItemByName(String itemName) {
		Item item = storeItems.get(itemName);
		if(item==null)
			throw new NoSuchElementException("No item found with name: " + itemName);
		
		storeItems.remove(itemName);
	}

	public void addMarkDown(String userInput) {
		String[] tokens = userInput.split(",");
		if(userInput.trim().length()==0 || tokens.length!=2) {
			throw new IllegalArgumentException("Use input format: 'name,discount'");
		}
		String productName = tokens[0].trim();
		double discount = 0;
		
		if (!storeItems.containsKey(productName))
			throw new NoSuchElementException("No item found with name: " + productName);
		if (markdowns.containsKey(productName))
			throw new IllegalArgumentException("Markdown already exists for that item: " + productName);
		
		try {
			discount = Double.valueOf(tokens[1]);
		} catch (NumberFormatException nfe) {
			throw new NumberFormatException ("Bad Discount value: " + nfe.getMessage());
		}
		
		markdowns.put(productName, discount);
	}
	
	public void removeMarkdownByName(String markdownName) {
		if (!markdowns.containsKey(markdownName))
			throw new NoSuchElementException("No markdown found with name: " + markdownName);
		
		markdowns.remove(markdownName);
	}

	public Map<String, Double> getMarkdowns() {
		return markdowns;
	}
	
	public Double getMarkdownByName(String productName) {
		return markdowns.get(productName);
	}

	public Buy_N_For_X_Special addSpecialBuy_N_For_X(String userInput) {
		String[] tokens = tokenizeInput_N_For_X_Special(userInput);
		String productName = tokens[0].trim();
		double quantity = Double.valueOf(tokens[1]);
		double price = Double.valueOf(tokens[2]);
		double limit = Double.valueOf(tokens[3]);
		
		checkProductNameIsEligibleForSpecial(productName);
		
		Buy_N_For_X_Special special_N_For_X = new Buy_N_For_X_Special(productName);
		special_N_For_X.setRequiredQuantity(quantity);
		special_N_For_X.setPrice(price);
		special_N_For_X.setLimit(limit);
		
		specials.put(productName, special_N_For_X);
		return special_N_For_X;
	}
	
	public Buy_N_Get_M_For_X_Special addSpecialBuy_N_Get_M_For_X(String userInput) {
		String[] tokens = tokenizeInput_Buy_N_Get_M_For_X_Special(userInput);
		String productName = tokens[0].trim();
		double nQuantity = Double.valueOf(tokens[1]);
		double mQuantity = Double.valueOf(tokens[2]);
		double percentDiscount = Double.valueOf(tokens[3]);
		double limit = Double.valueOf(tokens[4]);
		
		checkProductNameIsEligibleForSpecial(productName);
		
		Buy_N_Get_M_For_X_Special special_N_Get_M_For_X = new Buy_N_Get_M_For_X_Special(productName);
		special_N_Get_M_For_X.setRequiredQuantity(nQuantity);
		special_N_Get_M_For_X.setEligibleQuantity(mQuantity);
		special_N_Get_M_For_X.setPercentageDiscount(percentDiscount);
		special_N_Get_M_For_X.setLimit(limit);
		
		specials.put(productName, special_N_Get_M_For_X);
		return special_N_Get_M_For_X;
	}

	public Map<String, Special> getSpecials() {
		return specials;
	}
	
	public Special getSpecialsByName(String name) {
		return specials.get(name);
	}
	
	public void removeSpecialByName(String specialName) {
		if (!specials.containsKey(specialName))
			throw new NoSuchElementException("No special found with name: " + specialName);
		
		specials.remove(specialName);
	}

	private static String[] tokenizeInput_N_For_X_Special(String input) {
		String[] tokens = input.split(",");
		if(input.trim().length()==0 || tokens.length!=4) {
			throw new IllegalArgumentException("Use input format: 'productName,quantity,price,limit'");
		}
		return tokens;
	}
	
	private static String[] tokenizeInput_Buy_N_Get_M_For_X_Special(String input) {
		String[] tokens = input.split(",");
		if(input.trim().length()==0 || tokens.length!=5) {
			throw new IllegalArgumentException("Use input format: 'productName,nQuantity,mQuantity,price,limit'");
		}
		return tokens;
	}
	
	private void checkProductNameIsEligibleForSpecial(String productName) {
		if (!storeItems.containsKey(productName))
			throw new NoSuchElementException("No item found with name: " + productName);
		if (specials.containsKey(productName))
			throw new IllegalArgumentException("Special already exists for that item: " + productName);
	}
}
