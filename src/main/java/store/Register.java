package store;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import checkout.Buy_N_For_X_Special;
import checkout.Buy_N_Get_M_For_X_Special;
import checkout.ScannedItem;
import checkout.Special;
import product.Item;
import product.Measure;

public class Register {
	
	private ProductConfigurator configurator;
	private Map<String, ScannedItem> scannedItems;
	
	public Register(ProductConfigurator configurator) {
		scannedItems = new HashMap<>();
		this.configurator = configurator;
	}
	
	public ScannedItem scanItem(String userInput) {
		String[] tokens = userInput.split(",");
		if(tokens.length==1)
			return scanItemWithName(tokens[0].trim());
		
		if(tokens.length==2)
			return scanItemWithNameAndQuantity(tokens[0].trim(), tokens[1]);
		
		throw new IllegalArgumentException("Scanned Item format: 'name' or 'name,quantity'");
	}
	
	private ScannedItem scanItemWithNameAndQuantity(String name, String qty) {
		double quantity = Double.valueOf(qty);
		if(quantity<=0)
			throw new IllegalArgumentException("Quantity must be greater than zero");
		
		ScannedItem scannedItem = scannedItems.get(name);
		if(scannedItem==null) {
			Item item = configurator.getItemByName(name);
			scannedItem = new ScannedItem(item);
		}
		
		scannedItem.increaseQuantity(quantity);
		calculateExtendedPrice(scannedItem);
		scannedItems.put(name, scannedItem);
		return scannedItem;
	}
	
	private ScannedItem scanItemWithName(String name) {
		ScannedItem scannedItem = scannedItems.get(name);
		if(scannedItem!=null) {
			returnErrorIfSoldByWeight(scannedItem.getItem());
			scannedItem.increaseQuantity(1.0);
		} else {
			Item item = configurator.getItemByName(name);
			returnErrorIfSoldByWeight(item);
			scannedItem = new ScannedItem(item);
			scannedItem.setQuantity(1.0);
		}
		
		calculateExtendedPrice(scannedItem);
		scannedItems.put(name, scannedItem);
		return scannedItem;
	}

	private void returnErrorIfSoldByWeight(Item item) {
		if(item.getMeasure()==Measure.LB)
			throw new IllegalArgumentException(item.getName() + " must be scanned with a name and weight");
	}

	public Map<String, ScannedItem> getScannedItems() {
		return scannedItems;
	}
	
	public double getCheckoutTotal() {
		double total = 0;
		for(Map.Entry<String, ScannedItem> entry: scannedItems.entrySet())
			total += entry.getValue().getExtendedPrice();

		return total;
	}

	public Double calculateExtendedPrice(ScannedItem scannedItem) {
		scannedItem.setExtendedQuantity(0.0);
		scannedItem.setExtendendPrice(0.0);
		
		scannedItem = incrementExtendedFieldsForSpecial(scannedItem);
		scannedItem = incrementExtendedFieldsForMarkdown(scannedItem);
		
		double quantityToCalculate = getQuantityToCalculate(scannedItem);
				
		if(quantityToCalculate > 0) {
			double price = scannedItem.getItem().getPrice();
			double extendedPrice = scannedItem.getExtendedPrice();
			
			extendedPrice += price * quantityToCalculate;
			scannedItem.setExtendendPrice(extendedPrice);
			scannedItem.setExtendedQuantity(scannedItem.getQuantity());
		}
		return scannedItem.getExtendedPrice();
	}

	private ScannedItem incrementExtendedFieldsForMarkdown(ScannedItem scannedItem) {
		Double quantityToCalculate = getQuantityToCalculate(scannedItem);
		if (quantityToCalculate > 0) {
			String productName = scannedItem.getItem().getName();
			double price = scannedItem.getItem().getPrice();
			if (configurator.getMarkdowns().containsKey(productName)) {
				double discount = configurator.getMarkdowns().get(productName);
				double extendedPrice = scannedItem.getExtendedPrice();
				extendedPrice += (price - discount) * quantityToCalculate;
				scannedItem.setExtendendPrice(extendedPrice);
				scannedItem.setExtendedQuantity(scannedItem.getQuantity());
			}
		}
		return scannedItem;
	}
	
	
	private ScannedItem incrementExtendedFieldsForSpecial(ScannedItem scannedItem) {
		Special special = configurator.getSpecialsByName(scannedItem.getItem().getName());
		if (special instanceof Buy_N_For_X_Special) {
			Buy_N_For_X_Special special_Buy_N_For_X = (Buy_N_For_X_Special) special;
			scannedItem = incrementExtendedFieldsBuy_N_For_X_Special(special_Buy_N_For_X, scannedItem);
		} else if (special instanceof Buy_N_Get_M_For_X_Special) {
			Buy_N_Get_M_For_X_Special special_Buy_N_For_X = (Buy_N_Get_M_For_X_Special) special;
			scannedItem = incrementExtendedFieldsBuy_N_Get_M_For_X_Off_Special(special_Buy_N_For_X, scannedItem);
		}
		return scannedItem;
	}
	

	private ScannedItem incrementExtendedFieldsBuy_N_For_X_Special(Buy_N_For_X_Special special_N_For_X,
			ScannedItem scannedItem) {
		double quantity = scannedItem.getQuantity();
		double requiredQuantity = special_N_For_X.getRequiredQuantity();
		if (quantity >= requiredQuantity) {
			int multiples = computeEligibleMultiples(scannedItem, special_N_For_X);
			
			scannedItem.setExtendendPrice(multiples * special_N_For_X.getPrice());
			scannedItem.setExtendedQuantity(multiples * requiredQuantity);
		}
		return scannedItem;
	}
	
	private int computeEligibleMultiples(ScannedItem scannedItem, Buy_N_For_X_Special special) {
		double quantity = scannedItem.getQuantity();
		double requiredQuantity = special.getRequiredQuantity();
		double limit = special.getLimit();
		if (limit == 0)
			return (int) (quantity / requiredQuantity);
		
		return (int) Math.min((int) (quantity / requiredQuantity), (int) limit / requiredQuantity);
	}

	private ScannedItem incrementExtendedFieldsBuy_N_Get_M_For_X_Off_Special(
			Buy_N_Get_M_For_X_Special special_Buy_N_Get_M_For_X, ScannedItem scannedItem) {
		double quantity = scannedItem.getQuantity();
		double requiredQuantity = special_Buy_N_Get_M_For_X.getRequiredQuantity();
		double eligibleQuantity = special_Buy_N_Get_M_For_X.getEligibleQuantity();
		double limit = special_Buy_N_Get_M_For_X.getLimit();

		if (quantity >= requiredQuantity) {
			double price = scannedItem.getItem().getPrice();
			double discountQuantity = calculateDiscountQuantity(quantity, requiredQuantity, eligibleQuantity, limit);
			double discountExtendedPrice = discountQuantity * price * (1 - special_Buy_N_Get_M_For_X.getPercentageDiscount());
			double regularQuantity = calculateRegularQuantity(quantity, requiredQuantity, eligibleQuantity, limit);
			double regularExtendedPrice = regularQuantity * price;
			scannedItem.setExtendendPrice(regularExtendedPrice + discountExtendedPrice);
			scannedItem.setExtendedQuantity(regularQuantity + discountQuantity);
		}
		return scannedItem;
	}

	private double getQuantityToCalculate(ScannedItem scannedItem) {
		return scannedItem.getQuantity()-scannedItem.getExtendedQuantity();
	}
	
	private double calculateDiscountQuantity(double quantity, double requiredQuantity, double eligibleQuantity, double limit) {
		double discountQuantity = 0;
		if(limit==0)
			limit = quantity;
		
		while(quantity-requiredQuantity > 0 && limit > 0) {
			quantity-=requiredQuantity;
			limit-=requiredQuantity;
			double minimumEligibleQuantity = Math.min(quantity, eligibleQuantity);
			for(int i=0; i < minimumEligibleQuantity; i++) {
				double maxDiscountQuantityIncrement = Math.min(1, quantity--);
				discountQuantity += maxDiscountQuantityIncrement;
				limit-=maxDiscountQuantityIncrement;
			}
		}
		return discountQuantity;
	}
	
	private double calculateRegularQuantity(double quantity, double requiredQuantity, double eligibleQuantity, double limit) {
		double regularQuantity = 0;
		if(limit==0)
			limit = quantity;
		
		while(quantity-requiredQuantity > 0 && limit > 0) {
			regularQuantity+=requiredQuantity;
			quantity-=requiredQuantity+eligibleQuantity;
			limit-=requiredQuantity+eligibleQuantity;
		}
		return regularQuantity;
	}

	public void removeItemByName(String name) {
		if(!scannedItems.containsKey(name))
			throw new NoSuchElementException("Item not in checkout cart: " + name);
		
		scannedItems.remove(name);
	}
	
}
