package ui;

import java.util.Scanner;

import store.Register;
import store.ProductConfigurator;

public class MainMenu {

	public void loadMenu(Register register, ProductConfigurator configurator, Scanner scanner) {
		boolean flag = true;
		while (flag) {
			try {
				System.out.println("Main Menu:\n  1) Items\n  2) Markdowns\n  3) Specials\n  4) Checkout\n  5) Exit");
				String option = scanner.nextLine().trim();
				switch (option) {
				case "1":
					new StoreMenu().loadMenu(configurator, scanner);
					break;
				case "2":
					new MarkdownMenu().loadMenu(configurator, scanner);
					break;
				case "3":
					new SpecialMenu().loadMenu(configurator, scanner);
					break;
				case "4":
					new RegisterMenu().loadMenu(register, scanner);
					break;
				case "5":
				default:
					System.out.println("Exiting store...");
					flag = false;

				}
			} catch (RuntimeException re) {
				System.out.println(re.toString());
			}
		}
	}
}
