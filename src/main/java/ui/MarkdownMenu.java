package ui;

import java.util.Map;
import java.util.Scanner;

import store.ProductConfigurator;

public class MarkdownMenu {
	
	public void loadMenu(ProductConfigurator configurator, Scanner scanner) {
		boolean flag = true;
		while (flag) {
			try {
				System.out.println("Markdown Menu:\n  1) Add Markdown\n  2) Remove Markdown\n  3) Print Markdowns\n  4) Main Menu");
				String option = scanner.nextLine().trim();
				switch (option) {
				case "1":
					System.out.println("Markdown entry: 'name,discount'");
					addMarkdownToStore(configurator, scanner);
					break;
				case "2":
					System.out.println("Markdown name to remove");
					removeMarkdownFromStore(configurator, scanner);
					break;
				case "3":
					System.out.println("Markdowns");
					printMarkdowns(configurator);
					break;
				case "4": default:
					flag = false;
				}
			} catch (RuntimeException re) {
				System.out.println(re.toString());
			}
		}
	}

	private void addMarkdownToStore(ProductConfigurator configurator, Scanner scanner) {
		String userInput = scanner.nextLine();
		try {
			while (isNotBlank(userInput)) {
				configurator.addMarkDown(userInput);
				System.out.println("Markdown added");
				userInput = scanner.nextLine();
			}
		} catch (RuntimeException re) {
			System.out.println(re.toString() + " while adding mardown");
		}
	}
	
	private void removeMarkdownFromStore(ProductConfigurator configurator, Scanner scanner) {
		String userInput = scanner.nextLine();
		try {
			configurator.removeMarkdownByName(userInput);
		} catch (RuntimeException re) {
			System.out.println(re.toString() + " while removing markdown from store");
		}
	}
	
	private void printMarkdowns(ProductConfigurator configurator) {
		Map<String, Double> markdowns = configurator.getMarkdowns();
		for(Map.Entry<String, Double> entry: markdowns.entrySet())
			System.out.println(entry.getKey() + " discount $" + entry.getValue());
	}
	
	private boolean isNotBlank(String value) {
		return value!=null && value.trim().length()>0;
	}

}
