package ui;

import java.text.NumberFormat;
import java.util.Scanner;

import store.Register;

public class RegisterMenu {
	
	private static NumberFormat roughCurrencyFormatter = NumberFormat.getCurrencyInstance();

	public void loadMenu(Register register, Scanner scanner) {
		boolean flag = true;
		while (flag) {
			try {
				System.out.println(
						"Register Menu:\n  1) Scan an item\n  2) Remove an item\n  3) Display total\n  4) Main Menu");
				String option = scanner.nextLine().trim();
				switch (option) {
				case "1":
					System.out.println("Scan entry: 'name' or 'name,quantity'");
					scanItem(register, scanner);
					break;
				case "2":
					System.out.println("Remove entry: 'name'");
					removeScannedItem(register, scanner);
					break;
				case "3":
					displayCheckoutTotal(register);
					break;
				case "4":
				default:
					System.out.println("Main Menu");
					flag = false;
				}
			} catch (RuntimeException re) {
				System.out.println(re.toString());
			}
		}
	}

	private void scanItem(Register register, Scanner scanner) {
		String userInput = scanner.nextLine();
		try {
			while (isNotBlank(userInput)) {
				register.scanItem(userInput);
				displayCheckoutTotal(register);
				userInput = scanner.nextLine();
			}
		} catch (RuntimeException re) {
			System.out.println(re.toString());
		}
	}

	private void removeScannedItem(Register register, Scanner scanner) {
		String name = scanner.nextLine();
		register.removeItemByName(name);
	}
	
	private void displayCheckoutTotal(Register register) {
		System.out.println("Checkout Total: " + 
				roughCurrencyFormatter.format(register.getCheckoutTotal()));
	}
	
	private boolean isNotBlank(String value) {
		return value != null && value.trim().length() > 0;
	}
}
