package ui;

import java.util.Map;
import java.util.Scanner;

import checkout.Special;
import store.ProductConfigurator;

public class SpecialMenu {

	public void loadMenu(ProductConfigurator configurator, Scanner scanner) {
		boolean flag = true;
		while (flag) {
			try {
				System.out.println(
						"Special Menu:\n  1) Add Special\n  2) Remove Special\n  3) Print Special\n  4) Main Menu");
				String option = scanner.nextLine().trim();
				switch (option) {
				case "1":
					new SpecifySpecialMenu().loadMenu(configurator, scanner);
					break;
				case "2":
					System.out.println("Special name to remove");
					removeSpecialFromStore(configurator, scanner);
					break;
				case "3":
					System.out.println("Specials");
					printSpecials(configurator);
					break;
				default:
					flag = false;
				}
			} catch (RuntimeException re) {
				System.out.println(re.toString());
			}
		}
	}
	
	private void removeSpecialFromStore(ProductConfigurator configurator, Scanner scanner) {
		String userInput = scanner.nextLine();
		configurator.removeSpecialByName(userInput);
	}

	private void printSpecials(ProductConfigurator configurator) {
		Map<String, Special> specials = configurator.getSpecials();
		for(Map.Entry<String, Special> entry: specials.entrySet())
			System.out.println(entry.getValue().toString());
		
	}
	
	class SpecifySpecialMenu {
		void loadMenu(ProductConfigurator configurator, Scanner scanner) {
			System.out.println("Choose Special:\n1) Add Buy N for $X\n2) Add Buy N Get M for %X Off");
			String option = scanner.nextLine().trim();
			try {
				switch (option) {
				case "1":
					System.out.println("Buy N for $X entry: 'name,quantity,discount,limit (0 for unlimited)'");
					addBuy_N_For_X_Special(configurator, scanner);
					break;
				case "2":
					System.out.println(
						"Buy N Get M for %X Off entry: 'name,nQuantity,mQuantity,percentDiscount,limit (0 for unlimited)");
					addBuy_N_Get_M_For_X_Special(configurator, scanner);
					break;
				default:
				}
			} catch (RuntimeException re) {
				System.out.println(re.toString());
			}
		}

		void addBuy_N_For_X_Special(ProductConfigurator configurator, Scanner scanner) {
			String userInput = scanner.nextLine();
			while (isNotBlank(userInput)) {
				try {
					Special special = configurator.addSpecialBuy_N_For_X(userInput);
					System.out.println("Special added: " + special.toString());
				} catch (RuntimeException re) {
					System.out.println(re.toString());
				}
				userInput = scanner.nextLine();
			}
		}

		void addBuy_N_Get_M_For_X_Special(ProductConfigurator configurator, Scanner scanner) {
			String userInput = scanner.nextLine();
			while (isNotBlank(userInput)) {
				try {
					Special special = configurator.addSpecialBuy_N_Get_M_For_X(userInput);
					System.out.println("Special added: " + special.toString());
				} catch (RuntimeException re) {
					System.out.println(re.toString());
				}
				userInput = scanner.nextLine();
			}
		}

		boolean isNotBlank(String value) {
			return value != null && value.trim().length() > 0;
		}
	}

}
