package ui;

import java.util.Map;
import java.util.Scanner;

import product.Item;
import store.ProductConfigurator;

public class StoreMenu {
	
	public void loadMenu(ProductConfigurator configurator, Scanner scanner) {
		boolean flag = true;
		while (flag) {
			try {
				System.out.println("Store Menu:\n  1) Add Store Item\n  2) Remove Store Item\n  3) Print Store Items\n  4) Main Menu");
				String option = scanner.nextLine().trim();
				switch (option) {
				case "1":
					System.out.println("Product entry: 'name,measure(each/lb),price'");
					addProductToStore(configurator, scanner);
					break;
				case "2":
					System.out.println("Product name to remove");
					removeProductFromStore(configurator, scanner);
					break;
				case "3":
					System.out.println("Products in store");
					printStoreItems(configurator);
					break;
				case "4": default:
					flag = false;
				}
			} catch (RuntimeException re) {
				System.out.println(re.toString());
			}
		}
	}

	public Item addProductToStore(ProductConfigurator configurator, Scanner scanner) {
		String userInput = scanner.nextLine();
		Item item = null;
		try {
			while (isNotBlank(userInput)) {
				item = configurator.enterNewItem(userInput);
				configurator.addItemToStore(item);
				System.out.println("Added to store: " + item.toString());
				userInput = scanner.nextLine();
			}
		} catch (RuntimeException re) {
			System.out.println(re.toString() + " while adding product to store");
		}

		return item;
	}
	
	
	private void removeProductFromStore(ProductConfigurator configurator, Scanner scanner) {
		String userInput = scanner.nextLine();
		try {
			configurator.removeItemByName(userInput);
		} catch (RuntimeException re) {
			System.out.println(re.toString() + " while removing product from store");
		}
	}
	
	private void printStoreItems(ProductConfigurator configurator) {
		Map<String, Item> items = configurator.getItems();
		for(Map.Entry<String, Item> entry: items.entrySet()) {
			System.out.println(entry.getValue());
		}
	}

	private static boolean isNotBlank(String value) {
		return value!=null && value.trim().length()>0;
	}

}
