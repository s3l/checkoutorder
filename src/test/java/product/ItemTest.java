package product;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ItemTest {
	
	String errorMessage = "";
	
	@Test
	public void createItem_withNameMeasureAndPrice() {
		Item item = new Item("soup", Measure.EACH);
		item.setPrice(2.00);
		assertEquals("soup", item.getName());
		assertEquals(Measure.EACH, item.getMeasure());
		assertEquals(2.00, item.getPrice(), .00001);
	}
	
	@Test
	public void createItemWithBlankOrNullName_throwsIllegalArgumentException() {
		try {
			new Item("", Measure.EACH);
		} catch (IllegalArgumentException iae) {
			errorMessage = iae.getMessage();
		}
		assertEquals("Item name cannot be null or blank", errorMessage);
	}
	
	@Test
	public void createItemWithNullMeasure_throwsIllegalArgumentException() {
		try {
			new Item("soup", null);
		} catch (IllegalArgumentException iae) {
			errorMessage = iae.getMessage();
		}
		assertEquals("Item measure cannot be null", errorMessage);
	}

}

