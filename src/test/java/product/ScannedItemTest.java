package product;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import checkout.ScannedItem;

class ScannedItemTest {

	@Test
	public void addingQuantityWithDecimalToUnitPricedItems_roundsQuantityDown() {
		Item item = new Item("soup", Measure.EACH);
		item.setPrice(2.00);
		ScannedItem scannedItem = new ScannedItem(item);
		scannedItem.setQuantity(1.59);
		assertEquals(1.0, scannedItem.getQuantity(), .00001);
	}

}
