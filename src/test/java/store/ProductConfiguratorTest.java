package store;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Map;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import checkout.Buy_N_For_X_Special;
import checkout.Buy_N_Get_M_For_X_Special;
import product.Item;
import product.Measure;
import store.ProductConfigurator;

class ProductConfiguratorTest {
	
	private ProductConfigurator configurator;
	private Item soup;
	private Item bacon;
	private String errorMessage = "";
	
	@BeforeEach
	public void setup() {
		soup = new Item("soup", Measure.EACH);
		soup.setPrice(1.50);
		bacon = new Item("bacon", Measure.LB);
		bacon.setPrice(2.99);
		configurator = new ProductConfigurator();
	}
	
	@Nested
	class CreateNewItem {
		@Test
		public void enterNewItemWithBlankUserInput_throwIllegalArgumentException() {
			try {
				configurator.enterNewItem(" ");
			} catch (IllegalArgumentException iae) {
				errorMessage = iae.getMessage();
			}
			assertEquals("Use input format: 'name,measure,price'", errorMessage);
			
		}

		@Test
		public void enterNewItemWithOtherThan3CsvInputs_throwIllegalArgumentException() {
			try {
				configurator.enterNewItem("name,price");
			} catch (IllegalArgumentException iae) {
				errorMessage = iae.getMessage();
			}
			assertEquals("Use input format: 'name,measure,price'", errorMessage);
		}
		
		@Test
		public void enterNewItemWithValidString_addsNewItemToMap() {
			Item item = configurator.enterNewItem("beef,lb,5.99");
			assertEquals("beef",item.getName());
			assertEquals(Measure.LB, item.getMeasure());
			assertEquals(5.99, item.getPrice(), .00001);
		}
		
		@Test
		public void enterNewItemWithBadPrice_throwIllegalArgumentException() {
			try {
				configurator.enterNewItem("beef,lb,asdf");
			} catch (IllegalArgumentException iae) {
				errorMessage = iae.getMessage();
			}
			assertEquals("For input string: \"asdf\"", errorMessage);
		}
		
		@Test
		public void enterNewItemWithBadMeasure_throwIllegalArgumentException() {
			try {
				configurator.enterNewItem("beef,mlb,2.79");
			} catch (IllegalArgumentException iae) {
				errorMessage = iae.getMessage();
			}
			assertEquals("No enum constant product.Measure.MLB", errorMessage);
		}
	}
	
	@Nested
	class StoreAdditonRetrievalAndRemoval {
		
		@Test
		public void zeroOrLessItemPrice_throwsIllegalStateExceptionWhenAddedToItems() {
			soup.setPrice(0.0);
			try {
				configurator.addItemToStore(soup);
			} catch (IllegalStateException ise) {
				errorMessage = ise.getMessage();
			}
			assertEquals("Price must be greater than zero", errorMessage);
		}
		
		@Test
		public void addingItemWithDuplicateName_throwIllegalArgumentException() {
			configurator.addItemToStore(soup);
			try {
				configurator.addItemToStore(soup);
			} catch (IllegalArgumentException iae) {
				errorMessage = iae.getMessage();
			}
			assertEquals("Item already exists with that name", errorMessage);
		}
		
		@Test
		public void getItemByNameNotInStore_returnsItemNotFound() {
			try {
				configurator.getItemByName("soup");
			} catch (NoSuchElementException nse) {
				errorMessage = nse.getMessage();
			}
			assertEquals("No item found with name: " + "soup", errorMessage);
		}
		
		@Test
		public void getItemByName_returnsItemFromStoreList() {
			configurator.addItemToStore(soup);
			Item item = configurator.getItemByName("soup");
			assertEquals("soup", item.getName());
		}
		
		@Test
		public void removeItemByName_removesItemFromStoreList() {
			configurator.addItemToStore(soup);
			Item item = configurator.getItemByName("soup");
			assertEquals("soup", item.getName());
			assertTrue(configurator.getItems().size()==1);
			
			configurator.removeItemByName("soup");
			assertTrue(configurator.getItems().size()==0);
		}
		
	}
	
	
	@Nested
	class MarkdownAdditionRetrievalAndRemoval {
		
		@Test
		public void addMarkdownToItemNotInStore_returnsNoSuchElementException() {
			try {
				configurator.addMarkDown("soup,0.20");
			} catch (NoSuchElementException nse) {
				errorMessage = nse.getMessage();
			}
			assertEquals("No item found with name: soup", errorMessage);
		}
		
		@Test
		public void addSecondMarkdownForSameProduct_returnsIllegalArgumentException() {
			configurator.addItemToStore(soup);
			configurator.addMarkDown("soup,0.20");
			try {
				configurator.addMarkDown("soup,0.25");
			} catch (IllegalArgumentException iae) {
				errorMessage = iae.getMessage();
			}
			assertEquals("Markdown already exists for that item: soup", errorMessage);
		}
		
		@Test
		public void addDiscountToMarkdowns() {
			configurator.addItemToStore(soup);
			configurator.addMarkDown("soup,0.20");
			Map<String, Double> markdowns = configurator.getMarkdowns();
			assertEquals(markdowns.get("soup"), 0.20, .00001);
		}
		
		@Test
		public void retrieveMarkdownByName_returnsMarkdown() {
			configurator.addItemToStore(soup);
			configurator.addMarkDown("soup,0.20");
			double discount = configurator.getMarkdownByName("soup");
			assertEquals(0.20, discount, .00001);
		}
		
		@Test
		public void removeMarkdownByName_removesMarkdownFromStore() {
			configurator.addItemToStore(soup);
			configurator.addMarkDown("soup,0.20");
			double discount = configurator.getMarkdownByName("soup");
			assertEquals(0.20, discount, .00001);
			assertTrue(configurator.getMarkdowns().size()==1);
			configurator.removeMarkdownByName("soup");
			assertTrue(configurator.getMarkdowns().size()==0);
		}
	}
	
	@Nested
	class SpecialAdditionRetrievalAndRemoval {
		
		@Test
		public void addBuy_N_For_X_SpecialToItemNotInStore_returnsNoSuchElementException() {
			try {
				configurator.addSpecialBuy_N_For_X("soup,2,2,0");
			} catch (NoSuchElementException nse) {
				errorMessage = nse.getMessage();
			}
			assertEquals("No item found with name: soup", errorMessage);
		}
		
		@Test
		public void addSecondN_For_X_SpecialForSameProduct_returnsIllegalArgumentException() {
			configurator.addItemToStore(soup);
			configurator.addSpecialBuy_N_For_X("soup,4,3,0");
			try {
				configurator.addSpecialBuy_N_For_X("soup,2,2,0");
			} catch (IllegalArgumentException iae) {
				errorMessage = iae.getMessage();
			}
			assertEquals("Special already exists for that item: soup", errorMessage);
		}
		
		@Test
		public void retrieveN_For_X_SpecialByName_returnsSpecial() {
			configurator.addItemToStore(soup);
			configurator.addSpecialBuy_N_For_X("soup,4,3,0");
			Buy_N_For_X_Special actual = (Buy_N_For_X_Special) configurator.getSpecialsByName("soup");
			assertEquals("soup",actual.getProductName());
			assertEquals(4.0, actual.getRequiredQuantity(), .00001);
			assertEquals(3.0, actual.getPrice(), .00001);
		}
		
		@Test
		public void addBuy_N_Get_M_For_X_SpecialToItemNotInStore_returnsNoSuchElementException() {
			try {
				configurator.addSpecialBuy_N_Get_M_For_X("soup,2,2,.25,0");
			} catch (NoSuchElementException nse) {
				errorMessage = nse.getMessage();
			}
			assertEquals("No item found with name: soup", errorMessage);
		}
		
		@Test
		public void addSecondN_Get_M_For_X_SpecialForSameProduct_returnsIllegalArgumentException() {
			configurator.addItemToStore(soup);
			configurator.addSpecialBuy_N_Get_M_For_X("soup,2,2,.25,0");
			try {
				configurator.addSpecialBuy_N_Get_M_For_X("soup,3,2,.50,0");
			} catch (IllegalArgumentException iae) {
				errorMessage = iae.getMessage();
			}
			assertEquals("Special already exists for that item: soup", errorMessage);
		}
		
		@Test
		public void retrieveBuy_N_Get_M_For_X_SpecialByName_returnsSpecial() {
			configurator.addItemToStore(soup);
			configurator.addSpecialBuy_N_Get_M_For_X("soup,2,3,.25,0");
			Buy_N_Get_M_For_X_Special actual = (Buy_N_Get_M_For_X_Special) configurator.getSpecialsByName("soup");
			assertEquals("soup", actual.getProductName());
			assertEquals(2, actual.getRequiredQuantity(), .0001);
			assertEquals(3, actual.getEligibleQuantity(), .0001);
			assertEquals(.25, actual.getPercentageDiscount(), .0001);
		}
		
		@Test
		public void removeSpecialByName_removesSpecialFromList() {
			configurator.addItemToStore(soup);
			configurator.addSpecialBuy_N_Get_M_For_X("soup,2,3,.25,0");
			Buy_N_Get_M_For_X_Special special = (Buy_N_Get_M_For_X_Special) configurator.getSpecialsByName("soup");
			assertEquals("soup", special.getProductName());
			assertTrue(configurator.getSpecials().size()==1);
			configurator.getSpecialsByName("soup");
			assertTrue(configurator.getSpecials().size()==1);
		}
		
	}
}
