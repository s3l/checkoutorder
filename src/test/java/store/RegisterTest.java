package store;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import checkout.ScannedItem;
import product.Item;
import product.Measure;
import store.Register;
import store.ProductConfigurator;

class RegisterTest {
	
	private Register register;
	private ProductConfigurator configurator;
	private Item soup;
	private Item bacon;
	private String errorMessage = "";
	
	@BeforeEach
	public void commonSetup() {
		configurator = new ProductConfigurator();
		register = new Register(configurator);
		soup = new Item("soup", Measure.EACH);
		soup.setPrice(1.50);
		bacon = new Item("bacon", Measure.LB);
		bacon.setPrice(2.99);
	}
	
	@Nested
	class ScannedItemAdditionRetrievalAndRemoval {
		@Test
		public void scanItemWithZeroQuantityToCheckoutList_returnsIllegalArgumentException() {
			try {
				register.scanItem("soup,0");
			} catch (IllegalArgumentException iae) {
				errorMessage = iae.getMessage();
			}
			assertEquals("Quantity must be greater than zero", errorMessage);
		}
		
		@Test
		public void scanItemWith3Inputs_returnsIllegalArgumentException() {
			try {
				register.scanItem("soup,2,1");
			} catch (IllegalArgumentException iae) {
				errorMessage = iae.getMessage();
			}
			assertEquals("Scanned Item format: 'name' or 'name,quantity'", errorMessage);
		}
		
		@Test
		public void scanExistingItemWithQuantity2_incrementsQuantityFrom1To3() {
			configurator.addItemToStore(soup);
			register.scanItem("soup");
			register.scanItem("soup,2");
			Map<String, ScannedItem> scannedItems = register.getScannedItems();
			assertEquals(scannedItems.get("soup").getQuantity(), 3.0, .00001);
		}
		
		@Test
		public void scanNewUnitPricedItemWithoutQuantity_addsItemToScannedListWithQuantityOne() {
			configurator.addItemToStore(soup);
			register.scanItem("soup");
			Map<String, ScannedItem> scannedItems = register.getScannedItems();
			assertEquals(scannedItems.get("soup").getQuantity(), 1.0, .00001);
		}
		
		@Test
		public void scanExistingUnitPricedItemWithoutQuantity_incrementsItemInScannedListByQuantityOne() {
			configurator.addItemToStore(soup);
			register.scanItem("soup");
			register.scanItem("soup");
			Map<String, ScannedItem> scannedItems = register.getScannedItems();
			assertTrue(scannedItems.size()==1);
			assertEquals(scannedItems.get("soup").getQuantity(), 2.0, .00001);
		}
		
		@Test
		public void scanWeightPricedItemWithoutQuantity_returnsIllegalArgumentException() {
			configurator.addItemToStore(bacon);
			try {
				register.scanItem("bacon");
			} catch (IllegalArgumentException iae) {
				errorMessage = iae.getMessage();
			}
			assertEquals("bacon must be scanned with a name and weight", errorMessage);
		}
		
		@Test
		public void removeScannedItemFromCheckout_removesItemCompletely() {
			configurator.addItemToStore(soup);
			configurator.addItemToStore(bacon);
			register.scanItem("soup");
			register.scanItem("bacon,3");
			register.scanItem("bacon,0.5");
			
			register.removeItemByName("bacon");
			assertTrue(register.getScannedItems().size()==1);
			assertEquals("soup",register.getScannedItems().get("soup").getItem().getName());
		}
	}
	
	@Nested
	class CheckoutTotal {
		
		@Test
		public void addingPerUnitScannedItem_increasesCheckoutTotal() {
			configurator.addItemToStore(soup);
			register.scanItem("soup,2");
			Double total = configurator.getItemByName("soup").getPrice() * 2;
			assertEquals(total, register.getCheckoutTotal(), .00001);
		}
		
		@Test
		public void addingPerPoundScannedItem_increasesCheckoutTotal() {
			configurator.addItemToStore(bacon);
			register.scanItem("bacon,1.25");
			Double total = configurator.getItemByName("bacon").getPrice() * 1.25;
			assertEquals(total, register.getCheckoutTotal(), .00001);
		}
	}
	
	
	@Nested
	class CalculatingPrice {
		
		@BeforeEach
		public void setup() {
			configurator.addItemToStore(soup);
			configurator.addItemToStore(bacon);
		}
		
		@Test
		public void registersCalculateExtendedPriceMethod_setsExtendendPriceBasedOnPriceAndQuantity() {
			ScannedItem scannedItem = register.scanItem("soup,5");
			double extendedPrice = register.calculateExtendedPrice(scannedItem);
			assertEquals(soup.getPrice() * 5, extendedPrice, .00001);
		}
		
		@Nested
		class Markdowns {
			
			@Test
			public void registerCalculateUnitExtendedPriceWithMarkdown_discountsExtendedPriceBy20Cents() {
				configurator.addMarkDown("soup,0.20");
				ScannedItem scannedItem = register.scanItem("soup,1");
				double extendedPrice = register.calculateExtendedPrice(scannedItem);
				assertEquals(1.50-0.20, extendedPrice, .00001);
			}
			
			@Test
			public void registerCalculateWeightExtendedPriceWithMarkdown_discountsExtendedPriceBy20Cents() {
				configurator.addMarkDown("bacon,0.20");
				ScannedItem scannedItem = register.scanItem("bacon,1.875");
				double extendedPrice = register.calculateExtendedPrice(scannedItem);
				assertEquals((2.99-0.20) * 1.875, extendedPrice, .00001);
			}
		}
		
		@Nested
		class Specials {
			
			@Test
			public void registerCalculateUnitExtendedPriceBuy5for6Special_discountsExtendedPriceTo6() {
				configurator.addSpecialBuy_N_For_X("soup,5,6,0");
				ScannedItem scannedItem = register.scanItem("soup,5"); 
				double extendedPrice = register.calculateExtendedPrice(scannedItem);
				assertEquals(6.0, extendedPrice, .00001);
			}
			
			@Test
			public void registerCalculateWeightExtendedPriceBuy2for5SpecialWithPoint25Remainder_discountsExtendedPriceTo5Point75() {
				Double requiredQuantity = 2.0;
				Double scannedQuantity = 2.25;
				Double expected = 5.0 + ((scannedQuantity-requiredQuantity) * 2.99);
				configurator.addSpecialBuy_N_For_X("bacon,2,5,0");
				ScannedItem scannedItem = register.scanItem("bacon,2.25"); 
				double extendedPrice = register.calculateExtendedPrice(scannedItem);
				assertEquals(expected, extendedPrice, .00001);
			}
			
			@Test
			public void registerCalculateWeightPriceBuy3Get2HalfOffSpecialFor18Point5Pounds_discounts6Point5PoundsBy50Percent() {
				configurator.addSpecialBuy_N_Get_M_For_X("bacon,3,2,0.5,0");
				ScannedItem scannedItem = register.scanItem("bacon,18.5"); 
				double extendedPrice = register.calculateExtendedPrice(scannedItem);
				assertEquals(12*2.99 + 6.5*.5*2.99, extendedPrice, .00001);
			}
			
			@Test
			public void calculatePriceWhenRemainingWeightMarkedDownAfterBuyNGetMForXSpecial_returnsPriceWithSpecialAndMarkdownApplied() {
				double expected =
						(2.99 * 6) + 			//weight times regular price
						(2.99 * 0.4 * 4) +		//weight times special price
						((2.99 - 0.2) * 2.5);	//weight times markdown price	
				configurator.addSpecialBuy_N_Get_M_For_X("bacon,3,2,0.6,0");
				configurator.addMarkDown("bacon,.20");
				ScannedItem scannedItem = register.scanItem("bacon,12.5"); 
				double extendedPrice = register.calculateExtendedPrice(scannedItem);
				assertEquals(expected, extendedPrice, .00001);
			}
			
			@Test
			public void calculatePriceOnBuyNGetMForXSpecialWithLimit_restrictsOrderQuantityForDiscount() {
				double expected =
						(2.99 * 11) + 			//weight times regular price
						(2.99 * 0.6 * 4);		//weight times special price
				configurator.addSpecialBuy_N_Get_M_For_X("bacon,3,2,0.4,10");
				ScannedItem scannedItem = register.scanItem("bacon,15"); 
				double extendedPrice = register.calculateExtendedPrice(scannedItem);
				assertEquals(expected, extendedPrice, .00001);
			}
			
			@Test
			public void calculatePriceOnBuyNForXSpecialWithLimit_restrictsOrderQuantityForDiscount() {
				double expected =
						(2.99 * 8) + 			//weight times regular price
						(10.00 * 3);			//weight times special price
				configurator.addSpecialBuy_N_For_X("bacon,4,10,12");
				ScannedItem scannedItem = register.scanItem("bacon,20"); 
				double extendedPrice = register.calculateExtendedPrice(scannedItem);
				assertEquals(expected, extendedPrice, .00001);
			}
		}
	}
}

